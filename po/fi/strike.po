# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the maui-strike package.
# Tommi Nieminen <translator@legisign.org>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: maui-strike\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-21 00:45+0000\n"
"PO-Revision-Date: 2021-11-23 16:40+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tommi Nieminen"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "translator@legisign.org"

#: main.cpp:42
#, kde-format
msgid "Build and run code."
msgstr "Koosta ja suorita koodia."

#: main.cpp:42
#, kde-format
msgid "© 2021-%1 Camilo Higuita"
msgstr "© 2021–%1 Camilo Higuita"

#: main.cpp:44
#, kde-format
msgid "Camilo Higuita"
msgstr "Camilo Higuita"

#: main.cpp:44
#, kde-format
msgid "Developer"
msgstr "Kehittäjä"

#: main.qml:133
#, kde-format
msgid "Unsaved files"
msgstr "Tallentamattomat tiedostot"

#: main.qml:134
#, kde-format
msgid ""
"You have unsaved files. You can go back and save them or choose to discard "
"all changes and exit."
msgstr ""
"Tiedostoja on tallentamatta. Voit palata takaisin ja tallentaa ne tai hylätä "
"muutokset ja lopettaa."

#: main.qml:137 views/widgets/ProjectConfigDialog.qml:20
#, kde-format
msgid "Go back"
msgstr "Palaa takaisin"

#: main.qml:138
#, kde-format
msgid "Discard"
msgstr "Hylkää"

#: main.qml:245
#, kde-format
msgid "Toogle SideBar"
msgstr "Näytä tai piilota sivupaneeli"

#: views/editor/Editor.qml:88
#, kde-format
msgid "Open here"
msgstr "Avaa tähän"

#: views/editor/Editor.qml:98
#, kde-format
msgid "Open in new tab"
msgstr "Avaa uuteen välilehteen"

#: views/editor/Editor.qml:109
#, kde-format
msgid "Open in new split"
msgstr "Avaa uuteen jakoon"

#: views/editor/Editor.qml:121
#, kde-format
msgid "Cancel"
msgstr "Peru"

#: views/editor/EditorView.qml:34 views/editor/EditorView.qml:40
#, kde-format
msgid "Clone"
msgstr "Monista"

#: views/editor/EditorView.qml:35
#, kde-format
msgid "Enter the project Git URL"
msgstr "Anna projektin Git-verkko-osoite"

#: views/editor/EditorView.qml:59
#, kde-format
msgid "Missing Project"
msgstr "Projekti puuttuu"

#: views/editor/EditorView.qml:60
#, kde-format
msgid "Create or open a new project."
msgstr "Luo tai avaa uusi projekti."

#: views/editor/EditorView.qml:66 views/editor/NewFileDialog.qml:40
#, kde-format
msgid "Open Project"
msgstr "Avaa projekti"

#: views/editor/EditorView.qml:87
#, kde-format
msgid "Clone Project"
msgstr "Monista projekti"

#: views/editor/NewFileDialog.qml:11
#, kde-format
msgid "New file"
msgstr "Uusi tiedosto"

#: views/editor/NewFileDialog.qml:25
#, kde-format
msgid "Create New Project"
msgstr "Luo uusi projekti"

#: views/editor/NewFileDialog.qml:26 views/editor/NewFileDialog.qml:41
#, kde-format
msgid "Open a existing project"
msgstr "Avaa olemassa oleva projekti"

#: views/editor/NewFileDialog.qml:64
#, kde-format
msgid "Open File"
msgstr "Avaa tiedosto"

#: views/editor/NewFileDialog.qml:65
#, kde-format
msgid "Open one or multiple files"
msgstr "Avaa yksi tai useampia tiedostoja"

#: views/editor/NewFileDialog.qml:79
#, kde-format
msgid "New Template Source File"
msgstr "Uusi mallipohjalähdetiedosto"

#: views/editor/NewFileDialog.qml:80
#, kde-format
msgid "With support for basic text format editing"
msgstr "Perustekstimuotoilutuella"

#: views/PlacesSidebar.qml:68
#, kde-format
msgid "Project Sources"
msgstr "Projektin lähteet"

#: views/PlacesSidebar.qml:69
#, kde-format
msgid "Source files will be listed in here."
msgstr "Tässä luetellaan lähdetiedostot."

#: views/PlacesSidebar.qml:113
#, kde-format
msgid "Previous"
msgstr "Edellinen"

#: views/PlacesSidebar.qml:120
#, kde-format
msgid "Up"
msgstr "Ylemmäs"

#: views/PlacesSidebar.qml:128
#, kde-format
msgid "Next"
msgstr "Seuraava"

#: views/PlacesSidebar.qml:152
#, kde-format
msgid "Show Folders First"
msgstr "Näytä kansiot ensin"

#: views/PlacesSidebar.qml:162
#, kde-format
msgid "Type"
msgstr "Tyyppi"

#: views/PlacesSidebar.qml:171
#, kde-format
msgid "Date"
msgstr "Päiväys"

#: views/PlacesSidebar.qml:180
#, kde-format
msgid "Modified"
msgstr "Muutettu"

#: views/PlacesSidebar.qml:189
#, kde-format
msgid "Size"
msgstr "Koko"

#: views/PlacesSidebar.qml:198
#, kde-format
msgid "Name"
msgstr "Nimi"

#: views/PlacesSidebar.qml:210
#, kde-format
msgid "Group"
msgstr "Ryhmä"

#: views/widgets/BuildBar.qml:49
#, kde-format
msgid "Targets"
msgstr "Kohteet"

#: views/widgets/BuildBar.qml:135 views/widgets/ProjectConfigDialog.qml:14
#: views/widgets/ProjectConfigDialog.qml:66
#: views/widgets/ProjectConfigDialog.qml:177
#, kde-format
msgid "Configure"
msgstr "Asetukset"

#: views/widgets/BuildBar.qml:142
#, kde-format
msgid "Build"
msgstr "Koosta"

#: views/widgets/BuildBar.qml:149
#, kde-format
msgid "Run"
msgstr "Suorita"

#: views/widgets/BuildBar.qml:156
#, kde-format
msgid "Install"
msgstr "Asenna"

#: views/widgets/BuildBar.qml:298
#, kde-format
msgid "Split"
msgstr "Jaa"

#: views/widgets/BuildBar.qml:320
#, kde-format
msgid "Find and Replace"
msgstr "Etsi ja korvaa"

#: views/widgets/BuildBar.qml:333
#, kde-format
msgid "Line/Word Counter"
msgstr "Rivi- ja sanalaskuri"

#: views/widgets/BuildBar.qml:381
#, kde-format
msgid "Settings"
msgstr "Asetukset"

#: views/widgets/BuildBar.qml:392
#, kde-format
msgid "About"
msgstr "Tietoa"

#: views/widgets/ProjectConfigDialog.qml:20
#, kde-format
msgid "Abort"
msgstr "Keskeytä"

#: views/widgets/ProjectConfigDialog.qml:67
#, kde-format
msgid "Set the preferences to start hacking!"
msgstr "Tee asetusvalintasi ja ala koodata!"

#: views/widgets/ProjectConfigDialog.qml:81
#, kde-format
msgid "Build Directory"
msgstr "Koostamiskansio"

#: views/widgets/ProjectConfigDialog.qml:82
#, kde-format
msgid "Pick a build directory or keep the default one"
msgstr "Valitse koostamiskansio tai pidä nykyisellään"

#: views/widgets/ProjectConfigDialog.qml:86
#, kde-format
msgid "Build directory path"
msgstr "Koostamiskansion sijainti"

#: views/widgets/ProjectConfigDialog.qml:125
#, kde-format
msgid "Project"
msgstr "Projekti"

#: views/widgets/ProjectConfigDialog.qml:126
#, kde-format
msgid "Select the target project"
msgstr "Valitse kohdeprojekti"

#: views/widgets/ProjectConfigDialog.qml:178
#, kde-format
msgid "Finish"
msgstr "Viimeistele"

#: views/widgets/SettingsDialog.qml:14
#, kde-format
msgid "Editor"
msgstr "Muokkain"

#: views/widgets/SettingsDialog.qml:15
#, kde-format
msgid ""
"Configure the look and feel of the editor. The settings are applied globally"
msgstr "Aseta muokkaimen ulkoasu ja toiminta. Asetuksia käytetään yleisesti"

#: views/widgets/SettingsDialog.qml:19
#, kde-format
msgid "Auto Save"
msgstr "Automaattitallennus"

#: views/widgets/SettingsDialog.qml:20
#, kde-format
msgid "Auto saves your file every few seconds"
msgstr "Automaattitallentaa tiedoston muutaman sekunnin välein"

#: views/widgets/SettingsDialog.qml:31
#, kde-format
msgid "Line Numbers"
msgstr "Rivinumerot"

#: views/widgets/SettingsDialog.qml:32
#, kde-format
msgid "Display the line numbers on the left side"
msgstr "Näyttää rivinumerot vasemmalla"

#: views/widgets/SettingsDialog.qml:45
#, kde-format
msgid "Style"
msgstr "Tyyli"

#: views/widgets/SettingsDialog.qml:46
#, kde-format
msgid ""
"Configure the style of the syntax highliting. This configuration in not "
"applied for rich text formats"
msgstr ""
"Aseta syntaksikorostuksen tyyli. Asetusta ei käytetä muotoiltuun tekstiin"

#: views/widgets/SettingsDialog.qml:52
#, kde-format
msgid "Font Family"
msgstr "Fonttiperhe"

#: views/widgets/SettingsDialog.qml:65
#, kde-format
msgid "Font Size"
msgstr "Fonttikoko"

#: views/widgets/SettingsDialog.qml:77
#, kde-format
msgid "Tab Spacing"
msgstr "Sarkainväli"

#: views/widgets/SettingsDialog.qml:89
#, kde-format
msgid "Theme"
msgstr "Teema"

#: views/widgets/SettingsDialog.qml:90
#, kde-format
msgid "Editor color scheme style"
msgstr "Muokkaimen värimalli"

#: views/widgets/SettingsDialog.qml:108
#, kde-format
msgid "Color"
msgstr "Väri"

#: views/widgets/SettingsDialog.qml:109
#, kde-format
msgid "Editor background color"
msgstr "Muokkaimen taustaväri"
